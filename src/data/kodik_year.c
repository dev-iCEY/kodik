#include "../../include/kodik/data/kodik_year.h"
#include "../../lib/data/libkodik_year.h"

uint32_t
kodik_year_get_year(kodik_year_t const *p_object) {
    return libkodik_year_get_year(p_object);
}

uint32_t
kodik_year_get_count(kodik_year_t const *p_object) {
    return libkodik_year_get_count(p_object);
}
