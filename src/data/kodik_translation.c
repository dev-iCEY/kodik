#include "../../include/kodik/data/kodik_translation.h"
#include "../../lib/data/libkodik_translation.h"

char const *
kodik_translation_get_title(kodik_translation_t const *p_object) {
    return libkodik_translation_get_title(p_object);
}

uint32_t
kodik_translation_get_count(kodik_translation_t const *p_object) {
    return libkodik_translation_get_count(p_object);
}

uint32_t
kodik_translation_get_id(kodik_translation_t const *p_object) {
    return libkodik_translation_get_id(p_object);
}
