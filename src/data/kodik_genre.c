#include "../../include/kodik/data/kodik_genre.h"
#include "../../lib/data/libkodik_genre.h"

char const *
kodik_genre_get_title(kodik_genre_t const *p_object) {
    return libkodik_genre_get_title(p_object);
}

uint32_t
kodik_genre_get_count(kodik_genre_t const *p_object) {
    return libkodik_genre_get_count(p_object);
}
