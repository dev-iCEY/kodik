#include "../../include/kodik/data/kodik_country.h"
#include "../../lib/data/libkodik_country.h"

char const *
kodik_country_get_title(kodik_country_t const *p_object) {
    return libkodik_country_get_title(p_object);
}

uint32_t
kodik_country_get_count(kodik_country_t const *p_object) {
    return libkodik_country_get_count(p_object);
}
