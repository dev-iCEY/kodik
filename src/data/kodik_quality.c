#include "../../include/kodik/data/kodik_quality.h"
#include "../../lib/data/libkodik_quality.h"

char const *
kodik_quality_get_title(kodik_quality_t const *p_object) {
    return libkodik_quality_get_title(p_object);
}
