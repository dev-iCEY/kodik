#ifndef KODIK_LIBKODIK_HELPERS_H_DEFINED
#define KODIK_LIBKODIK_HELPERS_H_DEFINED 1

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

#include "../include/kodik/libkodik.h"

#define LIBKODIK_CHECK_POINTER(PTR, RETURN_VALUE)       \
if (NULL == (PTR)) {                                    \
    return RETURN_VALUE;                                \
}                                                       \

#define LIBKODIK_CHECK_PTR_BAD(PTR)                     \
        LIBKODIK_CHECK_POINTER(PTR, -10)                \

#define LIBKODIK_CHECK_PTR_NO_RET(PTR)                  \
        LIBKODIK_CHECK_POINTER(PTR,)

#define LIBKODIK_CHECK_PTR_NO_MEMORY(PTR)               \
        LIBKODIK_CHECK_POINTER(PTR, -11)                \



#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* KODIK_LIBKODIK_HELPERS_H_DEFINED */
