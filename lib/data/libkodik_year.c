#include "libkodik_year.h"
#include "../libkodik_helpers.h"
#include <stdlib.h>
#include <string.h>

struct kodik_year_t {
    uint32_t i_count;
    uint32_t i_year;
};

struct kodik_year_t *
libkodik_year_from_json(json_object const *p_object) {
    if (NULL == p_object) {
        return NULL;
    }
    json_object *p_obj_year = NULL;
    json_object *p_obj_count = NULL;
    json_bool result = json_object_object_get_ex(p_object, "year", &p_obj_year);
    if (0 == result) {
        return NULL;
    }
    result = json_object_object_get_ex(p_object, "count", &p_obj_count);
    if (0 == result) {
        return NULL;
    }
    uint32_t i_count    = json_object_get_uint64(p_obj_count);
    uint32_t i_year     = json_object_get_uint64(p_obj_year);
    return libkodik_year_new_data(i_year, i_count);
}

struct kodik_year_t *
libkodik_year_new(void) {
    struct kodik_year_t *p_object = malloc(sizeof(struct kodik_year_t));
    return NULL != p_object
            ? memset(p_object, 0, sizeof(struct kodik_year_t))
            : p_object;
}

struct kodik_year_t *
libkodik_year_new_data(uint32_t i_year, uint32_t i_count) {
    struct kodik_year_t *p_object = libkodik_year_new();
    if (NULL != p_object) {
        libkodik_year_set_data(p_object, i_year, i_count);
    }
    return p_object;
}

void
libkodik_year_free(struct kodik_year_t **pp_object) {
    LIBKODIK_CHECK_PTR_NO_RET(pp_object);
    struct kodik_year_t *p_object = (*pp_object);
    (*pp_object) = NULL;
    LIBKODIK_CHECK_PTR_NO_RET(p_object);
    free(p_object);
}

int32_t
libkodik_year_set_year(struct kodik_year_t *p_object, uint32_t i_year) {
    LIBKODIK_CHECK_PTR_BAD(p_object);
    p_object->i_year = i_year;
    return 0;
}

int32_t
libkodik_year_set_count(struct kodik_year_t *p_object, uint32_t i_count) {
    LIBKODIK_CHECK_PTR_BAD(p_object);
    p_object->i_count = i_count;
    return 0;
}

int32_t
libkodik_year_set_data(struct kodik_year_t *p_object, uint32_t i_year, uint32_t i_count) {
    LIBKODIK_CHECK_PTR_BAD(p_object);
    libkodik_year_set_count(p_object, i_count);
    libkodik_year_set_year(p_object, i_year);
    return 0;
}

uint32_t
libkodik_year_get_year(struct kodik_year_t const *p_object) {
    LIBKODIK_CHECK_POINTER(p_object, UINT32_MAX);
    return p_object->i_year;
}

uint32_t
libkodik_year_get_count(struct kodik_year_t const *p_object) {
    LIBKODIK_CHECK_POINTER(p_object, UINT32_MAX);
    return p_object->i_count;
}
