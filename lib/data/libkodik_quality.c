#include "libkodik_quality.h"
#include <stdlib.h>
#include <string.h>

struct kodik_quality_t {
    char *psz_title;
};

struct kodik_quality_t *
libkodik_quality_new_from_json(json_object const *p_object) {
    LIBKODIK_CHECK_POINTER(p_object, NULL);
    json_object *p_obj_title = NULL;
    if (0 == json_object_object_get_ex(p_object, "title", &p_obj_title)) {
        return NULL;
    }
    char const *p_obj_psz_title = json_object_get_string(p_obj_title);
    LIBKODIK_CHECK_POINTER(p_obj_psz_title, NULL);
    return libkodik_quality_new_data(p_obj_psz_title);
}

struct kodik_quality_t *
libkodik_quality_new(void) {
    struct kodik_quality_t *p_object = malloc(sizeof(struct kodik_quality_t));
    return NULL == p_object
            ? NULL
            : memset(p_object, 0, sizeof(struct kodik_quality_t));
}

struct kodik_quality_t *
libkodik_quality_new_data(char const *psz_title) {
    struct kodik_quality_t *p_object = libkodik_quality_new();
    LIBKODIK_CHECK_POINTER(p_object, NULL);
    if (0 != libkodik_quality_set_title(p_object, psz_title)) {
        libkodik_quality_free(&p_object);
    }
    return p_object;
}

void
libkodik_quality_free(struct kodik_quality_t **pp_object) {
    LIBKODIK_CHECK_PTR_NO_RET(pp_object);
    struct kodik_quality_t *p_object = (*pp_object);
    (*pp_object) = NULL;
    LIBKODIK_CHECK_PTR_NO_RET(p_object);
    free(p_object->psz_title);
    free(p_object);
}

int32_t
libkodik_quality_set_title(struct kodik_quality_t *p_object, char const *psz_title) {
    LIBKODIK_CHECK_PTR_BAD(p_object);
    p_object->psz_title = strdup(psz_title);
    LIBKODIK_CHECK_PTR_NO_MEMORY(p_object->psz_title);
    return 0;
}

char const *
libkodik_quality_get_title(struct kodik_quality_t const *p_object) {
    LIBKODIK_CHECK_POINTER(p_object, NULL);
    return p_object->psz_title;
}
