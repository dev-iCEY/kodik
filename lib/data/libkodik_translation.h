#ifndef KODIK_LIBKODIK_TRANSLATION_H_DEFINED
#define KODIK_LIBKODIK_TRANSLATION_H_DEFINED 1

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

#include "../libkodik_helpers.h"
#include <json-c/json.h>

struct kodik_translation_t;

struct kodik_translation_t *
libkodik_translation_new_from_json(json_object const *p_object);

struct kodik_translation_t *
libkodik_translation_new(void);

struct kodik_translation_t *
libkodik_translation_new_data(char const *psz_title, uint32_t const i_count, uint32_t const i_id);

void
libkodik_translation_free(struct kodik_translation_t **pp_object);

int32_t
libkodik_translation_set_data(struct kodik_translation_t *p_object, char const *psz_title, uint32_t const i_count, uint32_t const i_id);

int32_t
libkodik_translation_set_title(struct kodik_translation_t *p_object, char const *psz_title);

int32_t
libkodik_translation_set_id(struct kodik_translation_t *p_object, uint32_t const i_id);

int32_t
libkodik_translation_set_count(struct kodik_translation_t *p_object, uint32_t const i_count);

char const *
libkodik_translation_get_title(struct kodik_translation_t const *p_object);

uint32_t
libkodik_translation_get_id(struct kodik_translation_t const *p_object);

uint32_t
libkodik_translation_get_count(struct kodik_translation_t const *p_object);

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* KODIK_LIBKODIK_TRANSLATION_H_DEFINED */
