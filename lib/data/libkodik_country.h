#ifndef KODIK_LIBKODIK_COUNTRY_H_DEFINED
#define KODIK_LIBKODIK_COUNTRY_H_DEFINED 1

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

#include "../libkodik_helpers.h"
#include <json-c/json.h>

struct kodik_country_t;

struct kodik_country_t *
libkodik_country_new_from_json(json_object const *p_object);

struct kodik_country_t *
libkodik_country_new(void);

struct kodik_country_t *
libkodik_country_new_data(char const *psz_title, uint32_t const i_count);

void
libkodik_country_free(struct kodik_country_t **pp_object);

char const *
libkodik_country_get_title(struct kodik_country_t const *p_object);

uint32_t
libkodik_country_get_count(struct kodik_country_t const *p_object);

int32_t
libkodik_country_set_data(struct kodik_country_t *p_object, char const *psz_title, uint32_t const i_count);

int32_t
libkodik_country_set_title(struct kodik_country_t *p_object, char const *psz_title);

int32_t
libkodik_country_set_count(struct kodik_country_t *p_object, uint32_t const i_count);

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* KODIK_LIBKODIK_COUNTRY_H_DEFINED */
