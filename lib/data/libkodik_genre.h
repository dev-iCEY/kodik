#ifndef KODIK_LIBKODIK_GENRE_H_DEFINED
#define KODIK_LIBKODIK_GENRE_H_DEFINED 1

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

#include "../libkodik_helpers.h"
#include <json-c/json.h>

struct kodik_genre_t;

struct kodik_genre_t *
libkodik_genre_mew_fron_json(json_object const *p_object);

struct kodik_genre_t *
libkodik_genre_new(void);

struct kodik_genre_t *
libkodik_genre_new_data(char const *psz_title, uint32_t const i_count);

void
libkodik_genre_free(struct kodik_genre_t **pp_object);

int32_t
libkodik_genre_set_data(struct kodik_genre_t *p_object, char const *psz_title, uint32_t const i_count);

int32_t
libkodik_genre_set_title(struct kodik_genre_t *p_object, char const *psz_title);

int32_t
libkodik_genre_set_count(struct kodik_genre_t *p_object, uint32_t const i_count);

char const *
libkodik_genre_get_title(struct kodik_genre_t const *p_object);

uint32_t
libkodik_genre_get_count(struct kodik_genre_t const *p_object);

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* KODIK_LIBKODIK_GENRE_H_DEFINED */
