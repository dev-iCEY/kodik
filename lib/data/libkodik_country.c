#include "libkodik_country.h"
#include <stdlib.h>
#include <string.h>

struct kodik_country_t {
    char        *psz_title;
    uint32_t    i_count;
};

struct kodik_country_t *
libkodik_country_new_from_json(json_object const *p_object) {
    LIBKODIK_CHECK_POINTER(p_object, NULL);
    json_object *p_obj_title = NULL;
    json_object *p_obj_count = NULL;
    json_bool result = json_object_object_get_ex(p_object, "title", &p_obj_title);
    if (0 != result) {
        result = json_object_object_get_ex(p_object, "count", &p_obj_count);
        if (0 == result) {
            return NULL;
        }
    }
    char const *psz_title = json_object_get_string(p_obj_title);
    uint32_t const i_count = json_object_get_uint64(p_obj_count);
    return libkodik_country_new_data(psz_title, i_count);
}

struct kodik_country_t *
libkodik_country_new(void) {
    struct kodik_country_t *p_object = malloc(sizeof(struct kodik_country_t));
    return NULL == p_object
            ? NULL
            : memset(p_object, 0, sizeof(struct kodik_country_t));
}

struct kodik_country_t *
libkodik_country_new_data(char const *psz_title, uint32_t const i_count) {
    struct kodik_country_t *p_object = libkodik_country_new();
    LIBKODIK_CHECK_POINTER(p_object, NULL);
    if (0 != libkodik_country_set_data(p_object, psz_title, i_count)) {
        libkodik_country_free(&p_object);
    }
    return p_object;
}

void
libkodik_country_free(struct kodik_country_t **pp_object) {
    LIBKODIK_CHECK_PTR_NO_RET(pp_object);
    struct kodik_country_t *p_object = (*pp_object);
    (*pp_object) = NULL;
    LIBKODIK_CHECK_PTR_NO_RET(p_object);
    free(p_object->psz_title);
    free(p_object);
}

char const *
libkodik_country_get_title(struct kodik_country_t const *p_object) {
    LIBKODIK_CHECK_POINTER(p_object, NULL);
    return p_object->psz_title;
}

uint32_t
libkodik_country_get_count(struct kodik_country_t const *p_object) {
    LIBKODIK_CHECK_POINTER(p_object, UINT32_MAX);
    return p_object->i_count;
}

int32_t
libkodik_country_set_data(struct kodik_country_t *p_object, char const *psz_title, uint32_t const i_count) {
    LIBKODIK_CHECK_PTR_BAD(p_object);
    int32_t result = libkodik_country_set_count(p_object, i_count);
    if (0 == result) {
        result = libkodik_country_set_title(p_object, psz_title);
    }
    return result;
}

int32_t
libkodik_country_set_title(struct kodik_country_t *p_object, char const *psz_title) {
    LIBKODIK_CHECK_PTR_BAD(p_object);
    LIBKODIK_CHECK_PTR_BAD(psz_title);
    p_object->psz_title = strdup(psz_title);
    LIBKODIK_CHECK_PTR_NO_MEMORY(p_object->psz_title);
    return 0;
}

int32_t
libkodik_country_set_count(struct kodik_country_t *p_object, uint32_t const i_count) {
    LIBKODIK_CHECK_PTR_BAD(p_object);
    p_object->i_count = i_count;
    return 0;
}

