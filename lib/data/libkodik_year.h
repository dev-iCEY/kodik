#ifndef KODIK_LIBKODIK_YEAR_H_DEFINED
#define KODIK_LIBKODIK_YEAR_H_DEFINED 1

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

#include <json-c/json.h>

struct kodik_year_t;

struct kodik_year_t *
libkodik_year_from_json(json_object const *p_object);

struct kodik_year_t *
libkodik_year_new(void);

struct kodik_year_t *
libkodik_year_new_data(uint32_t i_year, uint32_t i_count);

void
libkodik_year_free(struct kodik_year_t **pp_object);

int32_t
libkodik_year_set_year(struct kodik_year_t *p_object, uint32_t i_year);

int32_t
libkodik_year_set_count(struct kodik_year_t *p_object, uint32_t i_count);

int32_t
libkodik_year_set_data(struct kodik_year_t *p_object, uint32_t i_year, uint32_t i_count);

uint32_t
libkodik_year_get_year(struct kodik_year_t const *p_object);

uint32_t
libkodik_year_get_count(struct kodik_year_t const *p_object);

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* KODIK_LIBKODIK_YEAR_H_DEFINED */
