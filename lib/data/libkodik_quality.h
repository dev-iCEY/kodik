#ifndef KODIK_LIBKODIK_QUALITY_H_DEFINED
#define KODIK_LIBKODIK_QUALITY_H_DEFINED 1

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

#include "../libkodik_helpers.h"
#include <json-c/json.h>

struct kodik_quality_t;

struct kodik_quality_t *
libkodik_quality_new_from_json(json_object const *p_object);

struct kodik_quality_t *
libkodik_quality_new(void);

struct kodik_quality_t *
libkodik_quality_new_data(char const *psz_title);

void
libkodik_quality_free(struct kodik_quality_t **pp_object);

int32_t
libkodik_quality_set_title(struct kodik_quality_t *p_object, char const *psz_title);

char const *
libkodik_quality_get_title(struct kodik_quality_t const *p_object);

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* KODIK_LIBKODIK_QUALITY_H_DEFINED */
