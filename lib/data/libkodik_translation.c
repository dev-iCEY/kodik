#include "libkodik_translation.h"
#include <stdlib.h>
#include <string.h>

struct kodik_translation_t {
    char        *psz_title;
    uint32_t    i_count;
    uint32_t    i_id;
};

struct kodik_translation_t *
libkodik_translation_new_from_json(json_object const *p_object) {
    LIBKODIK_CHECK_POINTER(p_object, NULL);
    json_object *p_obj_title = NULL;
    json_object *p_obj_id = NULL;
    json_object *p_obj_count = NULL;
    json_bool result = json_object_object_get_ex(p_object, "id", &p_obj_id);
    if (0 != result) {
        result = json_object_object_get_ex(p_object, "title", &p_obj_title);
        if (0 != result) {
            result = json_object_object_get_ex(p_object, "id", &p_obj_id);
            if (0 != result) {
                uint32_t const i_id = json_object_get_uint64(p_obj_id);
                char const *psz_title = json_object_get_string(p_obj_title);
                uint32_t const i_count = json_object_get_uint64(p_obj_count);
                return libkodik_translation_new_data(psz_title, i_count, i_id);
            }
        }
    }
    return NULL;
}

struct kodik_translation_t *
libkodik_translation_new(void) {
    struct kodik_translation_t *p_object = malloc(sizeof(struct kodik_translation_t));
    return NULL == p_object
            ? NULL
            : memset(p_object, 0, sizeof(struct kodik_translation_t));
}

struct kodik_translation_t *
libkodik_translation_new_data(char const *psz_title, uint32_t const i_count, uint32_t const i_id) {
    struct kodik_translation_t *p_object = libkodik_translation_new();
    LIBKODIK_CHECK_POINTER(p_object, NULL);
    if (0 == libkodik_translation_set_data(p_object, psz_title, i_count, i_id)) {
        libkodik_translation_free(&p_object);
    }
    return p_object;
}

void
libkodik_translation_free(struct kodik_translation_t **pp_object) {
    LIBKODIK_CHECK_PTR_NO_RET(pp_object);
    struct kodik_translation_t *p_object = (*pp_object);
    (*pp_object) = NULL;
    LIBKODIK_CHECK_PTR_NO_RET(p_object);
    free(p_object->psz_title);
    free(p_object);
}

int32_t
libkodik_translation_set_data(struct kodik_translation_t *p_object, char const *psz_title, uint32_t const i_count, uint32_t const i_id) {
    LIBKODIK_CHECK_PTR_BAD(p_object);
    LIBKODIK_CHECK_PTR_BAD(psz_title);
    int32_t result = libkodik_translation_set_title(p_object, psz_title);
    if (0 == result) {
        result = libkodik_translation_set_count(p_object, i_count);
        if (0 == result) {
            result = libkodik_translation_set_id(p_object, i_id);
        }
    }
    return result;
}

int32_t
libkodik_translation_set_title(struct kodik_translation_t *p_object, char const *psz_title) {
    LIBKODIK_CHECK_PTR_BAD(p_object);
    LIBKODIK_CHECK_PTR_BAD(psz_title);
    p_object->psz_title = strdup(psz_title);
    LIBKODIK_CHECK_PTR_NO_MEMORY(p_object);
    return 0;
}

int32_t
libkodik_translation_set_id(struct kodik_translation_t *p_object, uint32_t const i_id) {
    LIBKODIK_CHECK_PTR_BAD(p_object);
    p_object->i_id = i_id;
    return 0;
}

int32_t
libkodik_translation_set_count(struct kodik_translation_t *p_object, uint32_t const i_count) {
    LIBKODIK_CHECK_PTR_BAD(p_object);
    p_object->i_count = i_count;
    return 0;
}

char const *
libkodik_translation_get_title(struct kodik_translation_t const *p_object) {
    LIBKODIK_CHECK_POINTER(p_object, NULL);
    return p_object->psz_title;
}

uint32_t
libkodik_translation_get_id(struct kodik_translation_t const *p_object) {
    LIBKODIK_CHECK_POINTER(p_object, UINT32_MAX);
    return p_object->i_id;
}

uint32_t
libkodik_translation_get_count(struct kodik_translation_t const *p_object) {
    LIBKODIK_CHECK_POINTER(p_object, UINT32_MAX);
    return p_object->i_count;
}
