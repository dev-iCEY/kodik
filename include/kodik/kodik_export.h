
#ifndef KODIK_EXPORT_H
#define KODIK_EXPORT_H

#ifdef KODIK_STATIC_DEFINE
#  define KODIK_EXPORT
#  define KODIK_NO_EXPORT
#else
#  ifndef KODIK_EXPORT
#    ifdef kodik_EXPORTS
        /* We are building this library */
#      define KODIK_EXPORT __declspec(dllexport)
#    else
        /* We are using this library */
#      define KODIK_EXPORT __declspec(dllimport)
#    endif
#  endif

#  ifndef KODIK_NO_EXPORT
#    define KODIK_NO_EXPORT 
#  endif
#endif

#ifndef KODIK_DEPRECATED
#  define KODIK_DEPRECATED __attribute__ ((__deprecated__))
#endif

#ifndef KODIK_DEPRECATED_EXPORT
#  define KODIK_DEPRECATED_EXPORT KODIK_EXPORT KODIK_DEPRECATED
#endif

#ifndef KODIK_DEPRECATED_NO_EXPORT
#  define KODIK_DEPRECATED_NO_EXPORT KODIK_NO_EXPORT KODIK_DEPRECATED
#endif

#if 0 /* DEFINE_NO_DEPRECATED */
#  ifndef KODIK_NO_DEPRECATED
#    define KODIK_NO_DEPRECATED
#  endif
#endif

#endif /* KODIK_EXPORT_H */
