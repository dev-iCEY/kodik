#ifndef KODIK_KODIK_H_DEFINED
#define KODIK_KODIK_H_DEFINED 1

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

#include "libkodik.h"
#include "data/kodik_country.h"
#include "data/kodik_genre.h"
#include "data/kodik_quality.h"
#include "data/kodik_translation.h"
#include "data/kodik_year.h"

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* KODIK_KODIK_H_DEFINED */
