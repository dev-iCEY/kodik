#ifndef KODIK_LIBKODIK_H_DEFINED
#define KODIK_LIBKODIK_H_DEFINED

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

#include <stdint.h>
#include "kodik_export.h"

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* KODIK_LIBKODIK_H_DEFINED */
