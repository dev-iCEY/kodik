#ifndef KODIK_KODIK_COUNTRY_H_DEFINED
#define KODIK_KODIK_COUNTRY_H_DEFINED 1

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

#include "../libkodik.h"

typedef struct kodik_country_t kodik_country_t;

KODIK_EXPORT
char const *
kodik_country_get_title(kodik_country_t const *p_object);

KODIK_EXPORT
uint32_t
kodik_country_get_count(kodik_country_t const *p_object);

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* KODIK_KODIK_COUNTRY_H_DEFINED */
