#ifndef KODIK_KODIK_YEAR_H_DEFINED
#define KODIK_KODIK_YEAR_H_DEFINED 1

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

#include "../libkodik.h"
#include "../kodik_export.h"

typedef struct kodik_year_t kodik_year_t;

KODIK_EXPORT
uint32_t
kodik_year_get_count(kodik_year_t const *p_object);

KODIK_EXPORT
uint32_t
kodik_year_get_year(kodik_year_t const *p_object);

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* KODIK_KODIK_YEAR_H_DEFINED */
