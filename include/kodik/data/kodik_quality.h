#ifndef KODIK_KODIK_QUALITY_H_DEFINED
#define KODIK_KODIK_QUALITY_H_DEFINED 1

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

#include "../libkodik.h"

typedef struct kodik_quality_t kodik_quality_t;

KODIK_EXPORT
char const *
kodik_quality_get_title(kodik_quality_t const *p_object);

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* KODIK_KODIK_QUALITY_H_DEFINED */
