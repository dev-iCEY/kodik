#ifndef KODIK_KODIK_GENRE_H_DEFINED
#define KODIK_KODIK_GENRE_H_DEFINED 1

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

#include "../libkodik.h"

typedef struct kodik_genre_t kodik_genre_t;

KODIK_EXPORT
char const *
kodik_genre_get_title(kodik_genre_t const *p_object);

KODIK_EXPORT
uint32_t
kodik_genre_get_count(kodik_genre_t const *p_object);

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* KODIK_KODIK_GENRE_H_DEFINED */
