#ifndef KODIK_KODIK_TRANSLATION_H_DEFINED
#define KODIK_KODIK_TRANSLATION_H_DEFINED 1

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

#include "../libkodik.h"

typedef struct kodik_translation_t kodik_translation_t;

KODIK_EXPORT
char const *
kodik_translation_get_title(kodik_translation_t const *p_object);

KODIK_EXPORT
uint32_t
kodik_translation_get_count(kodik_translation_t const *p_object);

KODIK_EXPORT
uint32_t
kodik_translation_get_id(kodik_translation_t const *p_object);

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* KODIK_KODIK_TRANSLATION_H_DEFINED */
